function displaysection(){
	var $sectionList=$("li[id*='section-'] .content h3");
	$sectionList.each(function(){
		$(this).after('<a class="heideshow"><span class="fa fa-plus"></span></a>');
	})
	var $heideshow=$(".heideshow");
	$heideshow.each(function(index, el) {
		$(this).siblings("ul").hide();
		$(this).click(function(event) {
			if ($(this).siblings("ul").is(':hidden')) {
				$(this).addClass('active')
			}
			else{
				$(this).removeClass('active')
			}
			$(this).siblings("ul").toggle(500);
					
		});
		if (index==0) {
			$(this).click();
		};
	});
}
	
$(function(){
	if($(".editing")[0]){
		console.log("编辑模式");
	}else{
		displaysection();
	}
})