<?php
// This file is part of The Bootstrap 3 Moodle theme
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_dafei
 * @copyright  2014 kezhikeji, www.cloudkz.cn
 * @authors    Daixingwang
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();


function dafei_grid($hassidepre, $hassidepost) {

    if ($hassidepre && $hassidepost) {
        $regions = array('content' => 'col-sm-6 col-sm-push-3 col-lg-6 col-lg-push-3');
        $regions['pre'] = 'col-sm-3 col-sm-pull-6 col-lg-3 col-lg-pull-6';
        $regions['post'] = 'col-sm-3 col-lg-3';
    } else if ($hassidepre && !$hassidepost) {
        $regions = array('content' => 'col-sm-9 col-sm-push-3 col-lg-9 col-lg-push-3');
        $regions['pre'] = 'col-sm-3 col-sm-pull-9 col-lg-3 col-lg-pull-9';
        $regions['post'] = 'emtpy';
    } else if (!$hassidepre && $hassidepost) {
        $regions = array('content' => 'col-sm-9 col-lg-9');
        $regions['pre'] = 'empty';
        $regions['post'] = 'col-sm-3 col-lg-3';
    } else if (!$hassidepre && !$hassidepost) {
        $regions = array('content' => 'col-md-12');
        $regions['pre'] = 'empty';
        $regions['post'] = 'empty';
    }
    
    if ('rtl' === get_string('thisdirection', 'langconfig')) {
        if ($hassidepre && $hassidepost) {
            $regions['pre'] = 'col-sm-3  col-sm-push-3 col-lg-3 col-lg-push-3';
            $regions['post'] = 'col-sm-3 col-sm-pull-9 col-lg-3 col-lg-pull-9';
        } else if ($hassidepre && !$hassidepost) {
            $regions = array('content' => 'col-sm-9 col-lg-9');
            $regions['pre'] = 'col-sm-3 col-lg-3';
            $regions['post'] = 'empty';
        } else if (!$hassidepre && $hassidepost) {
            $regions = array('content' => 'col-sm-9 col-sm-push-3 col-lg-9 col-lg-push-3');
            $regions['pre'] = 'empty';
            $regions['post'] = 'col-sm-3 col-sm-pull-9 col-lg-3 col-lg-pull-9';
        }
    }
    return $regions;
}

/**
 * Loads the JavaScript for the zoom function.
 *
 * @param moodle_page $page Pass in $PAGE.
 */
/*function theme_dafei_initialise_zoom(moodle_page $page) {
    user_preference_allow_ajax_update('theme_bootstrap_zoom', PARAM_TEXT);
    $page->requires->yui_module('moodle-theme_bootstrap-zoom', 'M.theme_bootstrap.zoom.init', array());
}

/**
 * Get the user preference for the zoom function.
 */
/*
function theme_dafei_get_zoom() {
    return get_user_preferences('theme_bootstrap_zoom', '');
}
*/
