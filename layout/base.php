<?php
// This file is part of The Bootstrap 3 Moodle theme
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);

$knownregionpre = $PAGE->blocks->is_known_region('side-pre');
$knownregionpost = $PAGE->blocks->is_known_region('side-post');

$regions = dafei_grid($hassidepre, $hassidepost);
$PAGE->set_popup_notification_allowed(false);

//if ($knownregionpre || $knownregionpost) {
//    theme_dafei_initialise_zoom($PAGE);
//}
//$setzoom = theme_dafei_get_zoom();

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html(); ?>
    <link href="http://cdn.bootcss.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<nav role="navigation" class="navbar navbar-inverse incourse">
    <div class="container-fluid ">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#moodle-navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo $CFG->wwwroot;?>"><img src="<?php echo $OUTPUT->pix_url('logo', 'theme');?>" alt="<?php echo $SITE->shortname; ?>"> </a>
    </div>

    <div id="moodle-navbar" class="navbar-collapse collapse">

        <?php echo $OUTPUT->custom_menu(); ?>       
        <?php echo $OUTPUT->user_menu(); ?>
        <?php if ($knownregionpre || $knownregionpost) { ?>
            <div class="navbar-form navbar-right"><?php echo $OUTPUT->content_zoom(); ?></div>
        <?php } ?>
        <div class="navbar-form navbar-right"><?php echo $OUTPUT->page_heading_button(); ?></div>
        <ul class="nav navbar-right">
            <li><?php echo $OUTPUT->page_heading_menu(); ?></li>
        </ul>
    </div>
    </div>
</nav>
<header class="moodleheader">
    
</header>

<div id="page" class="container-fluid container incourse">
    <div id="page-navbar" class="clearfix row">
        <?php echo $OUTPUT->navbar(); ?>
    </div>
     <header id="page-header" class="clearfix f-bg row">
        <div id="course-header">
            <?php echo $OUTPUT->course_header(); ?>
            <div id="course-brief" class="container-fluid">
                <a href="<?php echo $CFG->wwwroot ?>" class="logo"></a> 
            </div> 
        </div>
    </header>
    <div id="top-nav" class="row">
        <?php echo $OUTPUT->blocks('banner'); ?>
    </div>
    <div id="page-content" class="row">
        <div class="f-bg clearfix">
        <div id="region-main" class="<?php echo $regions['content']; ?>">
            <?php echo $OUTPUT->blocks('main-top'); ?>
                <?php
                echo $OUTPUT->course_content_header();
                echo $OUTPUT->main_content();
                echo $OUTPUT->course_content_footer();
                ?>
                <?php echo $OUTPUT->blocks('main-bottom'); ?>
        </div>

        <?php
        if ($knownregionpre) {
            echo $OUTPUT->blocks('side-pre', $regions['pre']);
        }?>
        <?php
        if ($knownregionpost) {
            echo $OUTPUT->blocks('side-post', $regions['post']);
        }?>
    </div>
    </div>
</div>
 <footer id="page-footer">
    <div id="footerblock" class="container-fluid container text-left">
        <div id="footer-fluid">
            <div class="column">
                 <?php echo $OUTPUT->blocks('footer-fluid'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               <div class="column">
                        <?php echo $OUTPUT->blocks('footer-left'); ?>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="column">
                        <?php echo $OUTPUT->blocks('footer-middle'); ?>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="column">
                        <?php echo $OUTPUT->blocks('footer-right'); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    <div id="page-footer-link">
        <div class="container-fluid container">
        <div id="kezhi-footer" class="row">
            <div class="col-md-8">
                <ul class="list-inline text-left">
                    <li><?php echo $OUTPUT->home_link(); ?></li>
                    <li><a href="http://www.cloudkz.cn">关于我们</a></li>
                    <li><a href="http://moodle.cloudkz.cn">技术支持</a></li>
                    <li><?php echo $OUTPUT->page_doc_link(); ?></li>
                </ul>
                <div class="text-left">
                    <?php
                    echo $OUTPUT->login_info();
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="list-inline text-center">
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signupqq','theme'); ?>" alt="qq"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signuprenren','theme'); ?>" alt="人人"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signupweibo','theme'); ?>" alt="微博"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('weixin_btn','theme'); ?>" alt="微信"></a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    </footer>
    <div id="fixed-block">
        <div id="goto-top">
           <a class="goto-box text-center" href="#"><i class="fa fa-angle-up"></i></a> 
        </div>
    </div>
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
