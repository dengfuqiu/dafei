 <footer id="page-footer">
    <div id="footerblock" class="container-fluid container text-left">
        <div id="footer-fluid">
            <div class="column">
                 <?php echo $OUTPUT->blocks('footer-fluid'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               <div class="column">
                        <?php echo $OUTPUT->blocks('footer-left'); ?>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="column">
                        <?php echo $OUTPUT->blocks('footer-middle'); ?>
                </div>
            </div>
            <div class="col-md-4">
                 <div class="column">
                        <?php echo $OUTPUT->blocks('footer-right'); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    <div id="page-footer-link">
        <div class="container-fluid container">
        <div id="kezhi-footer" class="row">
            <div class="col-md-8">
                <ul class="list-inline text-left">
                    <li><?php echo $OUTPUT->home_link(); ?></li>
                    <li><a href="http://www.cloudkz.cn">关于我们</a></li>
                    <li><a href="http://moodle.cloudkz.cn">技术支持</a></li>
                    <li><?php echo $OUTPUT->page_doc_link(); ?></li>
                </ul>
                <div class="text-left">
                    <?php
                    echo $OUTPUT->login_info();
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="list-inline text-center">
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signupqq','theme'); ?>" alt="qq"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signuprenren','theme'); ?>" alt="人人"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('signupweibo','theme'); ?>" alt="微博"></a></li>
                    <li><a href="#"><img width="50px" src="<?php echo $OUTPUT->pix_url('weixin_btn','theme'); ?>" alt="微信"></a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    </footer>
    <div id="fixed-block">
        <div id="goto-top">
           <a class="goto-box text-center" href="#"><i class="fa fa-angle-up"></i></a> 
        </div>
    </div>